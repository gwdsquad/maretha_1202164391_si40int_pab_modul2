package com.example.maretha_1202164391_si40int_pab_modul2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
       Thread thread = new Thread(){
          @Override
        public void run() {
              try {
                  sleep(3000);
                  Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                  startActivity(intent);
                  finish();
              } catch (InterruptedException e) {
                  e.printStackTrace();
              }
          }
        };
        thread.start();
    }

}
