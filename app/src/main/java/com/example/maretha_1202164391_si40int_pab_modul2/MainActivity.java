package com.example.maretha_1202164391_si40int_pab_modul2;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    TextView calendar;
    Calendar mCurrentDate;
    int day, month, year;

    TextView time;
    Calendar currentTime;
    int hour, minute;

    TextView topup;
    Button topupbutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        calendar = findViewById(R.id.text_tanggalberangkat);
        mCurrentDate = Calendar.getInstance();
        time = findViewById(R.id.text_waktukeberangkatan);
        currentTime = Calendar.getInstance();
        topup = findViewById(R.id.text_saldo);
        topupbutton = findViewById(R.id.button_saldo);

        hour = currentTime.get(Calendar.HOUR_OF_DAY);
        minute = currentTime.get(Calendar.MINUTE);

        day = mCurrentDate.get(Calendar.DAY_OF_MONTH);
        month = mCurrentDate.get(Calendar.MONTH);
        year = mCurrentDate.get(Calendar.YEAR);

    }

    public void topup(View view) {
    }

    public void onClick(View view) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener(){
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    calendar.setText(dayOfMonth+"/"+(month+1)+"/"+year);
                }
            }, year, month, day);
            datePickerDialog.show();
    }

    public void waktu(View view) {

        TimePickerDialog timePickerDialog = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                time.setText(hourOfDay + ":" + minute);
            }
        }, hour, minute, true);
              timePickerDialog.show();
    }
    }

